// Funguje jako bi-directional Dictionary
// umožňuje lookup oběma směry

using System;
using System.Collections;
using System.Collections.Generic;

namespace Arnost.Collections
{
    public class Map<T1, T2> : IEnumerable<KeyValuePair<T1, T2>>
    {
        private readonly object _lock = new Object();
        private readonly Dictionary<T1, T2> _forward = new Dictionary<T1, T2>();
        private readonly Dictionary<T2, T1> _backward = new Dictionary<T2, T1>();

        public Map()
        {
        }

        public Map(IDictionary<T1, T2> dict)
        {
            if(dict == null)
                throw new ArgumentNullException(nameof(dict));
            
            foreach (var (key, value) in dict)
            {
                if(key == null || value == null)
                    throw new ArgumentException(
                        "Dictionary must not contain null elements", nameof(dict));
                
                _forward.Add(key, value);
                _backward.Add(value, key);
            }
        }

        public Map(IDictionary<T2, T1> dict)
        {
            if(dict == null)
                throw new ArgumentNullException(nameof(dict));
            
            foreach (var (key, value) in dict)
            {
                if(key == null || value == null)
                    throw new ArgumentException(
                        "Dictionary must not contain null elements", nameof(dict));
                
                _forward.Add(value, key);
                _backward.Add(key, value);
            }
        }

        public void Add(T1 value1, T2 value2)
        {
            if (value1 == null)
                    throw new ArgumentNullException(nameof(value1));

            if (value2 == null)
                throw new ArgumentNullException(nameof(value2));

            lock(_lock)
            {
                _forward.Add(value1, value2);
                _backward.Add(value2, value1);
            }
        }
        
        public bool TryAdd(T1 value1, T2 value2)
        {
            if(value1 == null)
                throw new ArgumentNullException(nameof(value1));
            
            if(value2 == null)
                throw new ArgumentNullException(nameof(value2));

            lock (_lock)
            {
                return
                    _forward.TryAdd(value1, value2) &&
                    _backward.TryAdd(value2, value1);
            }
        }

        public T2 GetForward(T1 key)
        {
            lock (_lock)
            {
                if (_forward.TryGetValue(key, out var result))
                    return result;
            }

            throw new KeyNotFoundException($"The given key '{key}' was not present in the map.");
        }

        public bool TryGetForward(T1 key, out T2 result)
        {
            lock(_lock)
                return _forward.TryGetValue(key, out result);
        }

        public T1 GetBackward(T2 key) 
        {
            lock(_lock)
            {
                if (_backward.TryGetValue(key, out var result))
                    return result;
            }
            throw new KeyNotFoundException($"The given key '{key}' was not present in the map.");
        }

        public bool TryGetBackward(T2 key, out T1 result)
        {
            lock(_lock)
                return _backward.TryGetValue(key, out result);
        }


        public IEnumerator<KeyValuePair<T1, T2>> GetEnumerator() 
            => _forward.GetEnumerator();
        

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}
