// Sada rozšíření pro IQueryable, slouží pro snazší filtrování dat z DB při 
// použití entity frameworku.
//
// Důležité je to, že pro výběr property je použitá Expression<T> a výsledkem
// je také expression - díky tomu z toho EF dokáže vygenerovat SQL.
public static class QueryableExtensions
{
    // .....

    public static IQueryable<T> PropertyOneOf<T,TArg>(this IQueryable<T> query, 
        IEnumerable<TArg> items, Expression<Func<T, TArg>> getProperty)
    {
        var entityParam = Expression.Parameter(typeof(T), "item");

        var capturedItems = CaptureVariable(items);
        var property = Expression.Invoke(getProperty, entityParam);

        var containsMethod = typeof(Enumerable)
            .GetMethods()
            .Where(x => x.Name == "Contains")
            .SingleOrDefault(x => x.GetParameters().Length == 2)
            ?.MakeGenericMethod(typeof(TArg));

        if (containsMethod == null)
            throw new InvalidOperationException(
                $"'Contains' method was not found on nameof(Enumerable)");

        return query.Where(
            Expression.Lambda<Func<T, bool>>(
                Expression.Call(containsMethod, capturedItems, property),
                entityParam));
    }

    private static MemberExpression CaptureVariable<T>(T value)
    {
        return Expression.Property(
            Expression.Constant(new { Value = value }), "Value");
    }

    // ......
    // Obdobným způsobem jsou implementovány další filtrovací metody
}


// Query bez použití extension metody
var query = _db.Users
    .Where(user => uids != null || uids.Contains(user.Uid))
    .Where(user => name != null || user.Name.Equals(name));

// Stejná query a použitá extension
var usersQuery = _db.Users
    .PropertyOneOf(userUids, user => user.Uid)
    .PropertyEqual(name, user => user.Name);

// Z tohoto krátkého výňatku to není tak patrné, ale při filtrování 
// podle většího množství fieldů je varianta s extensionami mnohem čitelnější.
